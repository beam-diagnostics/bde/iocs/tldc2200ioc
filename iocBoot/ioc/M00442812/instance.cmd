###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB")
epicsEnvSet("DEV_NAME", "DC1")
epicsEnvSet("USBVID", "0x1313")
epicsEnvSet("USBPID", "0x80C8")
epicsEnvSet("SERNO", "M00442812")
###############################################################################
